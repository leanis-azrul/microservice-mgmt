// Initialize dependencies
const pm2 = require('pm2')
const express = require('express')
const dotenv = require('dotenv')

// Load environmental variables
dotenv.config({ path: '.env' })
const port = process.env.EXPRESS_PORT

// Initialize app
const app = express()

// JSON parser
app.use(express.json())
app.use(express.urlencoded({ extended: true }))

// Define root route
app.get('/', function(req, res) {
  res.status(200).send('LeanPay Microservice Management')
});

app.get('/list', function(req, res) {
  pm2.connect(function(err) {
    if (err) {
      console.error(err);
      process.exit(2);
    } // Connect to PM2
    
    pm2.list(function(err, apps) {
        res.status(200).json(apps)
        console.log("ameena")
        pm2.disconnect();   // Disconnect from PM2
      if (err) {
        throw err
      }
    })
  })
})

//Start Server
app.listen(port, () => { 
  console.log(`Listening on port ${port}!`) 
})